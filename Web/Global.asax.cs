﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Raven.Client;
using Raven.Client.Embedded;

namespace Web
{
	public class MvcApplication : System.Web.HttpApplication
	{
		public MvcApplication()
		{
			BeginRequest += (sender, args) =>
				{
					System.Web.HttpContext.Current.Items["DocSession"] = _documentStore.OpenSession();
				};
			EndRequest += (sender, args) =>
				{
					var docSession = System.Web.HttpContext.Current.Items["DocSession"] as IDocumentSession;
					if (docSession != null)
					{
						if (Server.GetLastError() == null)
						{
							docSession.SaveChanges();
						}
						docSession.Dispose();
					}
				};
		}

		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();

			WebApiConfig.Register(GlobalConfiguration.Configuration);
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
			_documentStore = new EmbeddableDocumentStore() { ConnectionStringName = "RavenDB" };
			_documentStore.Conventions.IdentityPartsSeparator = "-";
			_documentStore.Initialize();
		}

		private static IDocumentStore _documentStore;
	}
}