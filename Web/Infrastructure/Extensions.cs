﻿using System;
using System.Linq;
using Raven.Client;
using Web.Models;

namespace Web.Infrastructure
{
	public static class IDocumentSessionExtensions
	{
		public static User GetUserByUsername(this IDocumentSession documentSession, string username)
		{
			if (string.IsNullOrWhiteSpace(username))
			{
				return null;
			}
			return documentSession.Query<User>().SingleOrDefault(u => u.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase));
		}
	}
}