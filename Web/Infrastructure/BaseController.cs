﻿using System.Collections.Generic;
using System.Web.Mvc;
using Raven.Client;

namespace Web.Infrastructure
{
	public abstract class BaseController : Controller
	{
		protected IDocumentSession DocumentSession { get { return System.Web.HttpContext.Current.Items["DocSession"] as IDocumentSession; } }
		private SessionManager _sessionManager;
		protected SessionManager SessionManager
		{
			get
			{
				if (_sessionManager == null)
				{
					_sessionManager = Session["SessionManager"] as SessionManager;
					if (_sessionManager == null)
					{
						Session.Add("SessionManager", (_sessionManager = new SessionManager()));
					}
				}
				return _sessionManager;
			}
		}

		protected void AddSuccessMessage(string text)
		{
			AddMessage(new Message() { Text = text, Type = MessageType.Success });
		}

		protected void AddErrorMessage(string text)
		{
			AddMessage(new Message() { Text = text, Type = MessageType.Error });
		}

		protected void AddMessage(string text, MessageType type)
		{
			AddMessage(new Message() { Text = text, Type = type });
		}

		protected void AddMessage(Message message)
		{
			var messages = ViewData["Messages"] as List<Message>;
			if (messages == null)
			{
				ViewData.Add("Messages", (messages = new List<Message>()));
			}
			messages.Add(message);
		}
	}
}
