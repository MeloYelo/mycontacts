﻿using Web.Models;

namespace Web.Infrastructure
{
	public class SessionManager
	{
		public User User { get; set; }
		public bool IsAuthenticated { get { return User != null; } }

		public void Clear()
		{
			User = null;
		}
	}
}