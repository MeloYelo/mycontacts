﻿using System;

namespace Web.Infrastructure
{
	public class Message
	{
		public string Text { get; set; }
		public MessageType Type { get; set; }
		public string CssClass
		{
			get
			{
				string cssClass = "alert";
				switch (Type)
				{
					case MessageType.Success:
						cssClass += " alert-success";
						break;
					case MessageType.Information:
						cssClass += " alert-info";
						break;
					case MessageType.Error:
						cssClass += " alert-error";
						break;
				}
				return cssClass;
			}
		}
	}

	public enum MessageType
	{
		Unknown = 0,
		Success,
		Information,
		Error
	}
}