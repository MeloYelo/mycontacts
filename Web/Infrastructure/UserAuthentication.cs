﻿using System.Security.Cryptography;
using System.Text;

namespace Web.Infrastructure
{
	public static class UserAuthentication
	{
		public static byte[] GenerateSaltedHash(string text, byte[] salt)
		{
			byte[] plainText = Encoding.UTF8.GetBytes(text);

			HashAlgorithm algorithm = new SHA256Managed();

			byte[] plainTextWithSaltBytes =
			  new byte[plainText.Length + salt.Length];

			for (int i = 0; i < plainText.Length; i++)
			{
				plainTextWithSaltBytes[i] = plainText[i];
			}
			for (int i = 0; i < salt.Length; i++)
			{
				plainTextWithSaltBytes[plainText.Length + i] = salt[i];
			}

			return algorithm.ComputeHash(plainTextWithSaltBytes);
		}

		public static byte[] GenerateRandomSalt()
		{
			byte[] returnValue = new byte[8];
			var generator = RNGCryptoServiceProvider.Create();
			generator.GetBytes(returnValue);
			return returnValue;
		} 
	}
}