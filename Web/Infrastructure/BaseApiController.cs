﻿using System.Web;
using System.Web.Http;
using Raven.Client;

namespace Web.Infrastructure
{
	public abstract class BaseApiController : ApiController
	{
		protected IDocumentSession DocumentSession { get { return HttpContext.Current.Items["DocSession"] as IDocumentSession; } }
		private SessionManager _sessionManager;
		protected SessionManager SessionManager
		{
			get
			{
				if (_sessionManager == null)
				{
					_sessionManager = HttpContext.Current.Session["SessionManager"] as SessionManager;
					if (_sessionManager == null)
					{
						HttpContext.Current.Session.Add("SessionManager", (_sessionManager = new SessionManager()));
					}
				}
				return _sessionManager;
			}
		}
	}
}