﻿namespace Web.Models
{
	public class User
	{
		public string Id { get; set; }
		public string Username { get; set; }
		public byte[] Password { get; set; }
		public byte[] Salt { get; set; }
	}
}