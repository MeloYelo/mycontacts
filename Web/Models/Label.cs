﻿namespace Web.Models
{
	public class Label
	{
		public string Id { get; set; }
		public string UserId { get; set; }
		public string Name { get; set; }
	}
}