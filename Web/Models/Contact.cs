﻿using System.Collections.Generic;

namespace Web.Models
{
	public class Contact
	{
		public Contact()
		{
			LabelIds = new List<string>();
		}

		public string Id { get; set; }
		public string UserId { get; set; }
		public string Name { get; set; }
		public string Email1 { get; set; }
		public string Email2 { get; set; }
		public string Phone1 { get; set; }
		public string Phone2 { get; set; }
		public string Address { get; set; }
		public List<string> LabelIds { get; set; }
		public bool IsDeleted { get; set; }
	}
}
