﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using Web.Infrastructure;
using Web.Models;

namespace Web.Controllers
{
	[AllowAnonymous]
	public class UserController : BaseController
	{
		public ActionResult Login()
		{
			if (User.Identity.IsAuthenticated)
			{
				DoLogout();
			}
			return View();
		}

		[HttpPost]
		public ActionResult Login(string username, string password)
		{
			if (!string.IsNullOrWhiteSpace(username) && !string.IsNullOrWhiteSpace(password))
			{
				var user = DocumentSession.GetUserByUsername(username);
				if (user != null && user.Password.SequenceEqual(UserAuthentication.GenerateSaltedHash(password, user.Salt)))
				{
					DoLogin(user);
					return RedirectToAction("Index", "Home");
				}
			}

			AddErrorMessage("Username/Password is not valid");

			ViewData["username"] = username;
			return View();
		}

		public ActionResult Create()
		{
			if (User.Identity.IsAuthenticated)
			{
				DoLogout();
			}
			return View();
		}

		[HttpPost]
		public ActionResult Create(string username, string password, string confirm)
		{
			string errorMessage = "Username is not valid";

			if (!string.IsNullOrWhiteSpace(username))
			{
				var user = DocumentSession.GetUserByUsername(username);
				if (user == null)
				{
					if (password.Length > 5)
					{
						if (password.Equals(confirm))
						{
							user = new User();
							user.Username = username;
							user.Salt = UserAuthentication.GenerateRandomSalt();
							user.Password = UserAuthentication.GenerateSaltedHash(password, user.Salt);
							DocumentSession.Store(user);
							DocumentSession.SaveChanges();
							DoLogin(user);
							AddSuccessMessage("User was created");
							return RedirectToAction("Index", "Home");
						}
						errorMessage = "Passwords do not match";
					}
					else
					{
						errorMessage = "Invalid Password (must be at least 6 characters long)";
					}
				}
				else
				{
					errorMessage = "Username already exists";
				}
			}

			AddErrorMessage(errorMessage);
			ViewData["username"] = username;

			return View();
		}

		public ActionResult Logout()
		{
			DoLogout();
			return RedirectToAction("Login", "User");
		}

		private void DoLogin(User user)
		{
			SessionManager.User = user;
			FormsAuthentication.SetAuthCookie(user.Username, true);
		}

		private void DoLogout()
		{
			Session.Clear();
			FormsAuthentication.SignOut();
		}
	}
}
