﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Web.Infrastructure;
using Web.Models;

namespace Web.Controllers
{
	public class ContactsController : BaseApiController
	{
		// GET api/contacts
		public IEnumerable<Contact> Get()
		{
			return DocumentSession.Query<Contact>().Where(c => c.UserId == SessionManager.User.Id && !c.IsDeleted);
		}

		// GET api/contacts/5
		public Contact Get(string id)
		{
			return DocumentSession.Load<Contact>(id);
		}

		// POST api/contacts
		public void Post([FromBody]Contact value)
		{
			DocumentSession.Store(value);
			DocumentSession.SaveChanges();
		}

		// PUT api/contacts/5
		public void Put(string id, [FromBody]Contact value)
		{
		}

		// DELETE api/contacts/5
		public void Delete(int id)
		{
		}
	}
}
