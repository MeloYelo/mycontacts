﻿using System.Web.Mvc;
using Web.Infrastructure;

namespace Web.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

    }
}
